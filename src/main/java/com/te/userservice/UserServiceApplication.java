package com.te.userservice;

import java.util.ArrayList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.te.userservice.entity.Role;
import com.te.userservice.entity.User;
import com.te.userservice.service.UserService;

@SpringBootApplication
//@EnableWebSecurity
//@Configuration
public class UserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.saveRole(new Role(101l, "ROLE_USER"));
			userService.saveRole(new Role(102l, "ROLE_MANAGER"));
			userService.saveRole(new Role(103l, "ROLE_ADMIN"));
			userService.saveRole(new Role(103l, "ROLE_SUPER_ADMIN"));

			userService.saveUser(new User(101l, "John Travolta", "john", "1234", new ArrayList<>()));
			userService.saveUser(new User(102l, "Will Smith", "will", "1234", new ArrayList<>()));
			userService.saveUser(new User(103l, "Jim Carry", "jim", "1234", new ArrayList<>()));
			userService.saveUser(new User(104l, "Arnold Schwarzenegger", "arnold", "1234", new ArrayList<>()));

		};
	}

}
