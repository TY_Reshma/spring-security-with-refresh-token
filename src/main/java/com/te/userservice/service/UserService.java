package com.te.userservice.service;

import java.util.List;

import com.te.userservice.entity.Role;
import com.te.userservice.entity.User;


public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User>getUsers();
}
